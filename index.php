<?php
/*
Plugin Name: BetterBackend
Description: Räum' dein Backend auf! BetterBackend stellt dir jede Menge Methoden zur Verfügung, mit denen du das Wordpress-Backend kinderleicht anpassen kannst.
Author: quäntchen + glück
Version: 1.2.6
Author URI: https://www.qundg.de/
*/

// Skript bei Direktaufruf killen
if ( ! defined( 'ABSPATH' ) ) {
    die();
}


if ( ! function_exists( 'debug' ) ) {

    /**
     * Prints out debug information about given variable.
     *
     * Only runs if debug level is greater than zero.
     *
     * @param mixed $var Variable to show debug information for.
     * @param bool $showHtml If set to true, the method prints the debug data in a browser-friendly way.
     * @param bool $showFrom If set to true, the method prints from where the function was called.
     *
     * @return void
     * @link http://book.cakephp.org/2.0/en/development/debugging.html#basic-debugging
     * @link http://book.cakephp.org/2.0/en/core-libraries/global-constants-and-functions.html#debug
     */
    function debug( $var = false, $showHtml = false, $showFrom = true ) {
        if ( ! defined( 'QUNDG_DEV_MODE' ) || QUNDG_DEV_MODE != true ) {
            return;
        }
        echo <<<EOF
        <style>
            .wp-admin>.qundg-debug {
                margin-left: 180px;
            }
            .qundg-debug {
                background: #C1D82F;
                color: #fff;
                overflow: auto;
                position: relative;
                border-radius: 10px;
                padding: 5px;
                margin: 1em;
            }
            .qundg-debug summary {
                outline: none;
                cursor: default;
            }
        </style>
EOF;
        echo '<details class="qundg-debug" open="open">';
        if ( $showFrom ) {
            $calledFrom = debug_backtrace();
            echo '<summary><strong>DEBUG: ';
            echo substr( $calledFrom[0]['file'], 1 );
            echo '</strong>';
            echo ' (Zeile <strong>' . $calledFrom[0]['line'] . '</strong>)<br />';
        }
        echo "</summary><pre>";
        $var = var_export( $var );
        if ( $showHtml ) {
            $var = str_replace( '<', '&lt;', str_replace( '>', '&gt;', $var ) );
        }
        echo $var . '</pre></details>';
    }
}


class BetterBackend {

    public static
        $done = array(),
        $remove_meta_boxes = array(),
        $tinymce_styles = array(),
        $tinymce_remove_buttons = array(
        1 => array(),
        2 => array(),
        3 => array(),
        4 => array(),
    ),
        $tinymce_add_buttons = array(
        1 => array(),
        2 => array(),
        3 => array(),
        4 => array(),
    ),
        $custom_admin_footer_text = '',
        $custom_dashboard = array(),
        $custom_videos = array(),
        $remove_admin_bar_menus = array(),
        $remove_post_order_forms = array(),
        $remove_post_publish_forms = array(),
        $add_menu_separators = array(),
        $remove_post_list_filters = array(),
        $remove_post_list_columns = array(),
        $add_post_list_columns = array(),
        $use_seo_features = array(),
        $use_meta_description_as_excerpt = array(),
        $disable_taxonomies = array(),
        $move_menu_items = array(),
        $remove_menu_items = array(),
        $add_menu_items = array(),
        $rename_menu_items = array(),
        $add_submenu_items = array(),
        $disable_rest_api = false;


    private function __construct() {
    } // Instanziierung verhindern


    /**
     * Ein Element oder ein Array aus Elementen einem anderen Array hinzufügen
     *
     * @param array $existing Bestehendes Array
     * @param array|mixed $new Einzelnes Element oder Array aus beliebigen Elementen, die $existing hinzugefügt werden
     *
     * @return array Vereinigung von $existing und $new
     */
    private static function join_array( $existing, $new ) {
        if ( is_array( $new ) ) {
            return array_merge( $existing, $new );
        } else {
            $existing[] = $new;

            return $existing;
        }
    }


    /**
     * Eine bestimmte Aufgabe nur einmal erledigen
     *
     * Benutzung: if(!done('id')) { ... }
     *
     * @param string $what Eindeutige Bezeichnung der Aktion
     *
     * @return boolean true, wenn die Aktion schon durchgeführt wurde, sonst false
     */
    private static function done( $what ) {
        if ( in_array( $what, self::$done ) ) {
            return true;
        } else {
            self::$done[] = $what;

            return false;
        }
    }


    /**
     * wp_head() aufräumen
     */
    public static function clean_wp_head() {
        remove_action( 'wp_head', 'wp_generator' ); // meta generator tag
        remove_action( 'wp_head', 'rsd_link' ); // really simple discovery
        remove_action( 'wp_head', 'feed_links', 2 ); // posts and comments feed
        remove_action( 'wp_head', 'index_rel_link' ); // index link
        remove_action( 'wp_head', 'wlwmanifest_link' ); // windows live writer manifest
        remove_action( 'wp_head', 'feed_links_extra', 3 ); // additional feeds
        remove_action( 'wp_head', 'start_post_rel_link' ); // start link
        remove_action( 'wp_head', 'parent_post_rel_link' ); // prev link
        remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head' ); // prev and next post
        remove_action( 'wp_head', 'locale_stylesheet' ); // localized stylesheet
        remove_action( 'wp_head', 'rel_canonical' ); // canonical url
        remove_action( 'wp_head', 'wp_shortlink_wp_head' ); // short url
        remove_action( 'wp_head', 'rest_output_link_wp_head', 10 ); // rest api link
        remove_action( 'wp_head', 'wp_oembed_add_discovery_links', 10 ); // oembed
        // Remove emoji script
        remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
        remove_action( 'wp_print_styles', 'print_emoji_styles' );
        add_filter( 'emoji_svg_url', '__return_false' );

        // Remove oEmbed-specific JavaScript from the front-end and back-end.
        remove_action( 'wp_head', 'wp_oembed_add_host_js' );


        // Inline Kommentarstyles aus wp_head schmeissen
        add_action(
            'widgets_init',
            function () {
                global $wp_widget_factory;
                remove_action(
                    'wp_head',
                    array( $wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style' )
                );
            }
        );

        if ( self::$disable_rest_api ) {
            add_filter(
                'rest_authentication_errors',
                function ( $access ) {
                    if ( ! is_user_logged_in() ) {
                        return new WP_Error(
                            'rest_cannot_access',
                            __( 'Only authenticated users can access the REST API.', 'disable-json-api' ),
                            array( 'status' => rest_authorization_required_code() )
                        );
                    }

                    return $access;

                }
            );


        }
    }


    /**
     * Feed-Links in wp_head packen
     */
    public static function add_feed_meta() {
        add_action(
            'wp_head',
            function () {
                echo '<link href="' . get_bloginfo(
                        'rss2_url'
                    ) . '" rel="alternate" title="RSS 2.0" type="application/rss+xml">' . "\n";
                echo '<link href="' . get_bloginfo(
                        'rss_url'
                    ) . '" rel="alternate" title="RSS .92" type="text/xml">' . "\n";
                echo '<link href="' . get_bloginfo(
                        'atom_url'
                    ) . '" rel="alternate" title="Atom 0.3" type="application/atom+xml">' . "\n";
            }
        );
    }


    /**
     * Überflüssige Meta-Boxen entfernen
     *
     * @param string $post_type Post-Typ, z.B. 'post' oder 'page'
     * @param mixed $box Slug der zu entfernenden Box (oder Array mit mehreren Boxen)
     */
    public static function remove_post_meta_box(
        $post_type,
        $box
    ) {
        // Sub-Array anlegen, falls noch keins für diesen Typ existiert
        if ( ! isset( self::$remove_meta_boxes[ $post_type ] ) ) {
            self::$remove_meta_boxes[ $post_type ] = array();
        }
        self::$remove_meta_boxes[ $post_type ] = self::join_array( self::$remove_meta_boxes[ $post_type ], $box );

        if ( ! self::done( 'remove_post_meta_box' ) ) {
            add_action(
                'admin_head',
                function () {
                    foreach ( BetterBackend::$remove_meta_boxes as $post_type => $boxes ) {
                        foreach ( $boxes as $box ) {
                            remove_meta_box( $box, $post_type, 'normal' );
                            remove_meta_box( $box, $post_type, 'side' );
                        }
                    }
                }
            );
        }
    }


    /**
     * TinyMCE Formatvorlage hinzufügen
     *
     * @deprecated seit 1.1.0, weil das Dropdown in einigen Browsern buggt
     */
    public static function tinymce_add_format(
        $title,
        $tag,
        $type,
        $classes = ''
    ) {
    }


    /**
     * Button aus TinyMCE entfernen
     *
     * @param int $row Zeile, aus der der Button entfernt wird
     * @param mixed $button Slug des zu entfernenden Buttons (oder Array aus Buttons)
     */
    public static function tinymce_remove_button(
        $row,
        $button
    ) {
        self::$tinymce_remove_buttons[ $row ] = self::join_array( self::$tinymce_remove_buttons[ $row ], $button );

        $filter = $row === 1 ? 'mce_buttons' : 'mce_buttons_' . $row; // die Filter heissen je nach Zeile mce_buttons bzw. mce_buttons2 bis mce_buttons4

        if ( ! self::done( 'tinymce_remove_buttons_' . $filter ) ) {
            add_filter(
                $filter,
                function ( $buttons ) {
                    $row = substr( current_filter(), - 1 );
                    if ( ! ctype_digit( $row ) ) // $row ist sonst "s" im Falle von "mce_buttons"
                    {
                        $row = 1;
                    }
                    $remove_buttons = BetterBackend::$tinymce_remove_buttons[ $row ];
                    foreach ( $remove_buttons as $button_name ) {
                        $key = array_search( $button_name, $buttons );
                        if ( $key !== false ) {
                            unset( $buttons[ $key ] );
                        }
                    }

                    return $buttons;
                }
            );
        }
    }


    /**
     * Button zu TinyMCE hinzufügen
     *
     * @param int $row Zeile, in der der Button eingefügt wird
     * @param string $button Slug des neuen Buttons
     * @param int|bool $position Position, an der der Button angefügt wird; false für "ans Ende"
     */
    public static function tinymce_add_button(
        $row,
        $button,
        $position = false
    ) {
        self::$tinymce_add_buttons[ $row ][] = array(
            'name'     => $button,
            'position' => $position,
        );
        $filter                              = $row === 1 ? 'mce_buttons' : 'mce_buttons_' . $row; // die Filter heissen je nach Zeile mce_button bzw. mce_buttons2 bis mce_buttons4

        if ( ! self::done( 'tinymce_add_buttons_' . $filter ) ) {
            add_filter(
                $filter,
                function ( $buttons ) {
                    $row = substr( current_filter(), - 1 );
                    if ( ! ctype_digit( $row ) ) // $row ist sonst "s" im Falle von "mce_buttons"
                    {
                        $row = 1;
                    }
                    $add_buttons = BetterBackend::$tinymce_add_buttons[ $row ];
                    foreach ( $add_buttons as $button ) {
                        if ( $button['position'] === false ) // am Ende hinzufügen, falls keine Position angegeben wurde
                        {
                            $buttons[] = $button['name'];
                        } else {
                            array_splice(
                                $buttons,
                                $button['position'],
                                0,
                                $button['name']
                            ); // sonst an gewünschter Position ins Array schieben
                        }
                    }

                    return $buttons;
                }
            );
        }
    }


    /**
     * Alle Meta-Boxen im Dashboard entfernen
     */
    public static function clean_dashboard() {
        remove_action( 'welcome_panel', 'wp_welcome_panel' );

        add_action(
            'wp_dashboard_setup',
            function () {
                remove_meta_box( 'dashboard_browser_nag', 'dashboard', 'normal' );
                remove_meta_box( 'dashboard_right_now', 'dashboard', 'normal' );
                remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );
                remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
                remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );
                remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
                remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'side' );
                remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );
                remove_meta_box( 'dashboard_secondary', 'dashboard', 'side' );
                remove_meta_box( 'dashboard_activity', 'dashboard', 'normal' );
            }
        );

        add_action(
            'admin_head',
            function () {
                $current_screen = get_current_screen();
                if ( $current_screen->base == 'dashboard' ) {
                    ?>
                    <style type="text/css">
                        .empty-container {
                            border: none !important;
                            height: auto !important;
                        }

                        .empty-container:after {
                            content: '' !important;
                        }
                    </style>
                    <?php
                }
            }
        );
    }


    /**
     * Eigene Meta-Box dem Dashboard hinzufügen
     *
     * @param string $url Zu verlinkende URL
     * @param string $text Linktext
     * @param string $icon Dashicon-Bezeichnung, z.B. dashicons-admin-post
     */
    public static function add_custom_dashboard_item(
        $url,
        $text,
        $icon = 'dashicons-admin-post'
    ) {
        self::$custom_dashboard[] = array(
            'url'  => $url,
            'text' => $text,
            'icon' => $icon,
        );

        if ( ! self::done( 'custom_dashboard' ) ) {
            add_action(
                'wp_dashboard_setup',
                function () {
                    add_meta_box(
                        'qundg-dashboard',
                        'Start',
                        array( __CLASS__, 'dashboard_welcome' ),
                        'dashboard',
                        'normal'
                    );
                }
            );
        }
    }

    public static function dashboard_welcome() // Callback zur Darstellung der Meta-Box
    {
        ?>
        <style type="text/css">
            #qundg-dashboard {
                display: none;
            }

            .qundg-dashboard-menu-item {
                padding-top: 6px;
                margin-left: 26px;
            }
        </style>
        <script>
            jQuery(document).ready(function($) {
                $('#qundg-dashboard').show().removeClass('postbox').find('.hndle, .handlediv').remove();
                $('#qundg-dashboard .postbox-header').remove();
            });
        </script>
        <h2>Hallo <?php echo ucwords( wp_get_current_user()->data->display_name ); ?></h2>
        <p>
            Schön, dass du da bist! Was nun?
        </p>
        <ul>
            <?php

            foreach ( self::$custom_dashboard as $item ) {
                ?>
                <li>
                    <div class="icon16 dashicons <?php echo $item['icon']; ?>"></div>
                    <p class="qundg-dashboard-menu-item">
                        <a href="<?php echo $item['url']; ?>"><?php echo $item['text']; ?></a>
                    </p>
                </li>
                <?php
            }

            ?>
        </ul>
        <?php
    }


    /**
     * Eigene Video-Meta-Box dem Dashboard hinzufügen
     *
     * @param string $headline Überschrift der Box
     * @param string $text Text unter dem Video
     * @param array $sources Quellen des Videos
     */
    public static function add_custom_dashboard_video(
        $headline,
        $text,
        $sources = array()
    ) {
        self::$custom_videos[] = array(
            'headline' => $headline,
            'text'     => $text,
            'sources'  => $sources,
        );
        if ( ! self::done( 'custom_videos' ) ) {
            add_action( 'wp_dashboard_setup', array( __CLASS__, 'dashboard_videos' ) );
        }
    }

    public static function dashboard_videos() { // Hook für die Erstellung der Metaboxen mit Video
        foreach ( self::$custom_videos as $k => $video ) {
            add_meta_box(
                'qundg-videos-' . $k,
                $video['headline'],
                array( __CLASS__, 'dashboard_video' ),
                'dashboard',
                'normal',
                'low',
                array( $video )
            );
        }
    }

    public static function dashboard_video(
        $post,
        $data
    ) { // Callback zur Erstellung einer Metabox mit Video
        $video = reset( $data['args'] );
        ?>
        <video width="100%" controls>
            <?php foreach ( $video['sources'] as $type => $source ): ?>
                <source src="<?php echo $source ?>" type="video/mp4">
            <?php endforeach ?>
            Ihr Browser unterstützt leider keine Videos
        </video>
        <p>
            <?php echo $video['text'] ?>
        </p>
        <ul>
            <li>
                <div class="icon16 dashicons dashicons-admin-media"></div>
                <p class="qundg-dashboard-menu-item">
                    <a href="<?php echo reset( $video['sources'] ) ?>" target="_blank">Video herunterladen</a>
                </p>
            </li>
        </ul>
        <?php
    }


    /**
     * Passwort-Änderungszeiten tracken und User an regelmäßige Änderung erinnern
     */
    public static function track_password_changes() {
        add_action(
            'wp_dashboard_setup',
            function () {
                add_meta_box(
                    'qundg-change-password',
                    'Passwort',
                    array( __CLASS__, 'dashboard_password_reminder' ),
                    'dashboard',
                    'side'
                );
            }
        );

        // Zeitpunkte von Passwortänderungen tracken
        // Validierung hat Wordpress an dieser Stelle schon erledigt
        add_action(
            'profile_update',
            function ( $user_id ) {
                if ( isset( $_POST['pass1'] ) and $_POST['pass1'] != '' ) {
                    $user_id = intval( $_POST['user_id'] );
                    update_option( 'qundg_last_password_change_user_' . $user_id, time() );
                }
            }
        );
    }

    public static function dashboard_password_reminder() // Callback zur Darstellung der Meta-Box
    {
        ?>
        <style type="text/css">
            #qundg-change-password {
                display: none;
            }

            .qundg-dashboard-menu-item {
                padding-top: 6px;
                margin-left: 26px;
            }
        </style>
        <script>
            jQuery(document).ready(function($) {
                $('#qundg-change-password').show().removeClass('postbox').find('.hndle, .handlediv').remove();
                $('#qundg-change-password .postbox-header').remove();
            });
        </script>
        <h2>Passwort ändern?</h2>
        <?php

        $last_password_change = get_option( 'qundg_last_password_change_user_' . wp_get_current_user()->ID );
        if ( $last_password_change ) {
            $last_password_change_days = floor( ( time() - $last_password_change ) / ( 60 * 60 * 24 ) );
            if ( $last_password_change_days == 0 ) {
                echo '<p>Du hast dein Passwort heute schon geändert &ndash; vielen Dank!</p>';

                return;
            }
        }

        ?>
        <p>
            Bitte denk dran, dein Passwort regelmäßig zu ändern.
            <?php

            if ( $last_password_change and $last_password_change_days != 0 ) {
                echo '<br>Die letzte Änderung war ';
                echo $last_password_change_days == 1
                    ? 'gestern'
                    : 'vor ' . $last_password_change_days . ' Tagen';
                echo '.';
            }

            ?>
        </p>
        <div class="icon16 dashicons dashicons-admin-network"></div>
        <p class="qundg-dashboard-menu-item">
            <a href="profile.php">Gute Idee, das mache ich sofort!</a>
        </p>
        <?php
    }


    /**
     * Element aus Admin-Leiste entfernen
     *
     * @param mixed $item Slug des zu entfernenden Elements (oder Array aus Elementen)
     */
    public static function remove_admin_bar_item(
        $item
    ) {
        self::$remove_admin_bar_menus = self::join_array( self::$remove_admin_bar_menus, $item );

        if ( ! self::done( 'remove_admin_bar_items' ) ) {
            add_action(
                'wp_before_admin_bar_render',
                function () {
                    global $wp_admin_bar;
                    foreach ( BetterBackend::$remove_admin_bar_menus as $item ) {
                        $wp_admin_bar->remove_menu( $item );
                    }
                }
            );
        }
    }


    /**
     * Eigene Nachricht im Admin-Footer anzeigen
     *
     * Blendet Wordpress-Dankesnachricht, Version und Update-Benachrichtigungen
     * aus und ersetzt sie durch einen eigenen Text.
     *
     * @param string $text Text, der im Footer angezeigt wird - kann HTML sein
     */
    public static function custom_admin_footer(
        $text = ''
    ) {
        if ( empty( $text ) ) {
            $text = '
                <a href="https://www.qundg.de/" target="_blank"><img src="' . plugins_url( 'qundg-logo.png',
                    __FILE__ ) . '" alt="quäntchen + glück"></a>
                <div style="float: right">
                    Fragen zur Website? Einfach <a href="https://www.qundg.de/kontakt/" target="_blank">anrufen</a> oder per Mail an <a href="mailto:jederzeit@qundg.de">jederzeit@qundg.de</a> nachfragen!
                </div>';
        }

        self::$custom_admin_footer_text = $text;

        // Update-Benachrichtigung aus Footer entfernen
        add_action(
            'admin_menu',
            function () {
                remove_filter( 'update_footer', 'core_update_footer' );
            }
        );

        add_action(
            'admin_init',
            function () {
                add_filter(
                    'admin_footer_text',
                    function () {
                        return BetterBackend::$custom_admin_footer_text;
                    },
                    999
                );
            }
        );
    }


    /**
     * Überflüssige Felder auf Profilseite entfernen
     */
    public static function clean_profile_edit_form() {
        add_action(
            'admin_head',
            function () {
                $current_screen = get_current_screen();
                if ( $current_screen->base == 'profile' or $current_screen->base == 'profile-user' ) {
                    ?>
                    <style type="text/css">
                        #your-profile h2,
                        #your-profile .user-user-login-wrap, /* Login-Name (kann eh nicht geändert werden) */
                        #your-profile .user-description-wrap, /* Biographische Angaben */
                        #your-profile .user-profile-picture, /* Profilbild */
                        #your-profile .user-nickname-wrap, /* Spitzname */
                        #your-profile .user-url-wrap, /* Website */
                        #your-profile .user-rich-editing-wrap, /* WYSIWYG verwenden? */
                        #your-profile .user-admin-color-wrap, /* Farbschema */
                        #your-profile .user-comment-shortcuts-wrap, /* Tastaturkürzel */
                        #your-profile .show-admin-bar.user-admin-bar-front-wrap /* Werkzeugleiste zeigen */
                        {
                            display: none;
                        }
                    </style>
                    <?php
                }
            }
        );
    }


    /**
     * Formularfeld für Post-Reihenfolge entfernen
     *
     * @param mixed $post_type Post-Typ (oder Array aus Post-Typen), für den das Feld entfern werden soll
     */
    public static function remove_post_order_form(
        $post_type
    ) {
        self::$remove_post_order_forms = self::join_array( self::$remove_post_order_forms, $post_type );

        if ( ! self::done( '$remove_post_order_forms' ) ) {
            add_action(
                'admin_head',
                function () {
                    $current_screen = get_current_screen();
                    if ( in_array( $current_screen->post_type, BetterBackend::$remove_post_order_forms ) ) {
                        ?>
                        <style type="text/css">
                            #pageparentdiv p:nth-last-child(1),
                            #pageparentdiv p:nth-last-child(2),
                            #pageparentdiv p:nth-last-child(3) {
                                display: none;
                            }
                        </style>
                        <?php
                    }
                }
            );
        }
    }


    /**
     * Bereiche aus der Publish-Meta-Box beim Bearbeiten von Posts entfernen
     *
     * @param string $post_type Post-Typ, für den der Bereich ausgeblendet werden soll
     * @param mixed $form_type Bereich, der ausgeblendet wird (oder Array aus Bereichen): status, visibility,
     *     schedule, save, preview
     */
    public static function remove_post_publish_form( $post_type, $form_type ) {
        // Sub-Array anlegen, falls noch keins für diesen Typ existiert
        if ( ! isset( self::$remove_post_publish_forms[ $post_type ] ) ) {
            self::$remove_post_publish_forms[ $post_type ] = array();
        }
        self::$remove_post_publish_forms[ $post_type ] = self::join_array(
            self::$remove_post_publish_forms[ $post_type ],
            $form_type
        );

        if ( ! self::done( 'remove_post_publish_forms' ) ) {
            add_action(
                'admin_head',
                function () {
                    // 'status' kann man sich leichter merken als '#submitdiv .misc-pub-post-status'
                    $mapping = array(
                        'status'     => '#submitdiv .misc-pub-post-status',
                        'visibility' => '#submitdiv .misc-pub-visibility',
                        'schedule'   => '#submitdiv .misc-pub-curtime',
                        'save'       => '#submitdiv #save-action',
                        'preview'    => '#submitdiv #preview-action',
                    );

                    $current_screen    = get_current_screen();
                    $current_post_type = $current_screen->post_type;

                    if ( array_key_exists( $current_post_type, BetterBackend::$remove_post_publish_forms ) ) {
                        foreach ( BetterBackend::$remove_post_publish_forms[ $current_post_type ] as $form_type ) {
                            if ( isset( $mapping[ $form_type ] ) ) {
                                ?>
                                <style type="text/css">
                                    <?php echo $mapping[$form_type]; ?>
                                    {
                                        display: none
                                    }
                                </style>
                                <?php
                            }
                        }
                    }
                }
            );
        }
    }


    /**
     * Benutzerrollen umdefinieren
     *
     * Redakteure können zusätzlich zu ihren normalen Rechte auch Theme-Optionen
     * bearbeiten (also vor allem Navigationsmenüs).Der neue "Super-Redakteur"
     * hat alle Redakteursrechte und kann zusätzlich User hinzufügen, bearbeiten
     * und entfernen. Die Rollen "Abonnent" und "Mitarbeiter" entfallen.
     *
     * Muss sofort beim Laden des Themes aufgerufen werden (nicht hooked).
     */
    public static function customize_user_roles() {
        global $wp_roles;
        #@TODO: fails for some reason? $wp_roles->use_db = false;

        $editor = get_role( 'editor' );
        $editor->add_cap( 'edit_theme_options' );

        $editor_capabilities          = $editor->capabilities;
        $additional_capabilities      = array(
            'list_users'         => true,
            'promote_users'      => true,
            'remove_users'       => true,
            'edit_users'         => true,
            'create_users'       => true,
            'delete_users'       => true,
            'edit_theme_options' => true,
        );
        $elevated_editor_capabilities = array_merge( $editor_capabilities, $additional_capabilities );

        add_role( 'elevated_editor', 'Super-Redakteur', $elevated_editor_capabilities );
        $elevated_editor = get_role( 'elevated_editor' );
        $elevated_editor->add_cap( 'edit_theme_options' );

        remove_role( 'contributor' );
        remove_role( 'subscriber' );

        add_filter(
            'editable_roles',
            function ( $roles ) {
                if ( ! current_user_can( 'administrator' ) ) {
                    unset( $roles['administrator'] );
                }

                return $roles;
            }
        );
    }


    /**
     * Benutzerrollen umdefinieren
     *
     * @deprecated seit 1.2.0, verwende stattdessen customize_user_roles()
     */
    public static function customize_user_roles_on_theme_change( $force_now = false ) {
        self::customize_user_roles();
    }


    /**
     * Features in der Post-Liste ausblenden
     *
     * @param string $post_type Post-Typ, zu dem ein Feature ausgeblendet werden soll - oder Sepzialtyp '_users'
     * @param mixed $feature Feature, das ausgeblendet wird (oder Array aus Features): filters, search, bulk_actions
     */
    public static function remove_post_list_feature( $post_type, $feature ) {
        // Sub-Array anlegen, falls noch keins für diesen Typ existiert
        if ( ! isset( self::$remove_post_list_filters[ $post_type ] ) ) {
            self::$remove_post_list_filters[ $post_type ] = array();
        }
        self::$remove_post_list_filters[ $post_type ] = self::join_array(
            self::$remove_post_list_filters[ $post_type ],
            $feature
        );

        if ( ! self::done( 'remove_post_list_filters' ) ) {
            add_action(
                'admin_head',
                function () {
                    // 'search' kann man sich leichter merken als '#posts-filter .search-box'
                    $mapping = array(
                        'filters'      => '.wrap .tablenav.top',
                        'search'       => '.wrap p.search-box',
                        'bulk_actions' => '.wrap .tablenav.bottom .bulkactions',
                    );

                    $current_screen    = get_current_screen();
                    $current_post_type = $current_screen->post_type;

                    // prüfen, ob für den aktuellen Post-Typ etwas ausgeblendet werden soll
                    if ( array_key_exists( $current_post_type, BetterBackend::$remove_post_list_filters )
                         or ( $current_screen->base == 'users' and isset( BetterBackend::$remove_post_list_filters['_users'] ) and $current_post_type = '_users' )
                    ) // 'users' ist kein Post-Typ, daher spezielle Abfrage nach '_users'
                    {
                        foreach ( BetterBackend::$remove_post_list_filters[ $current_post_type ] as $feature ) {
                            if ( isset( $mapping[ $feature ] ) ) {
                                ?>
                                <style type="text/css">
                                    <?php echo $mapping[$feature]; ?>
                                    {
                                        display: none
                                    ;
                                    }
                                </style>
                                <?php
                            }
                        }
                    }
                }
            );
        }
    }


    /**
     * Spalten in der Post-Liste ausblenden
     *
     * @param string $post_type Post-Typ, zu dem eine Spalte ausgeblendet werden soll
     * @param mixed $column Spalte, die ausgeblendet wird (oder Array aus Spalten), z.B. 'author'
     */
    public static function remove_post_list_column( $post_type, $column ) {
        // Sub-Array anlegen, falls noch keins für diesen Typ existiert
        if ( ! isset( self::$remove_post_list_columns[ $post_type ] ) ) {
            self::$remove_post_list_columns[ $post_type ] = array();
        }
        self::$remove_post_list_columns[ $post_type ] = self::join_array(
            self::$remove_post_list_columns[ $post_type ],
            $column
        );

        if ( ! self::done( 'remove_post_list_columns' ) ) {
            // alles an init hooken, erst dann ist self::$remove_post_list_columns definitiv vollständig
            add_action(
                'init',
                function () {
                    // jeder Post-Typ hat einen eigenen Filter, daher können wir nicht einfach eine zentrale Filterfunktion nehmen
                    foreach ( BetterBackend::$remove_post_list_columns as $post_type => $x ) {
                        $filter = 'manage_edit-' . $post_type . '_columns';
                        add_filter(
                            $filter,
                            function ( $columns ) {
                                $current_screen    = get_current_screen();
                                $current_post_type = $current_screen->post_type;

                                foreach ( BetterBackend::$remove_post_list_columns[ $current_post_type ] as $column ) {
                                    unset( $columns[ $column ] );
                                }

                                return $columns;
                            },
                            99
                        );
                    }
                }
            );
        }
    }


    /**
     * Der Post-Liste eine Spalte hinzufügen
     *
     * @param string $post_type Post-Typ, dem eine Spalte hinzugefügt werden soll
     * @param string $slug Eindeutiger Bezeichner der neuen Spalte, nur alphanumerische Zeichen und Underscores
     * @param string $title Titel der neuen Spalte
     * @param callable $callback Callback zur Darstellung des Spalteninhalts, erhält als Parameter die Post-ID
     */
    public static function add_post_list_column( $post_type, $slug, $title, $callback ) {
        self::$add_post_list_columns[ $post_type ][ $slug ] = array(
            'title'    => $title,
            'callback' => $callback,
        );

        if ( ! self::done( 'add_post_list_columns' ) ) {
            // alles an init hooken, erst dann ist self::$add_post_list_columns definitiv vollständig
            add_action(
                'init',
                function () {
                    // jeder Post-Typ hat einen eigenen Filter, daher können wir nicht einfach eine zentrale Filterfunktion nehmen
                    foreach ( BetterBackend::$add_post_list_columns as $post_type => $x ) {
                        $filter_for_title   = 'manage_edit-' . $post_type . '_columns';
                        $filter_for_content = 'manage_' . $post_type . '_posts_custom_column';

                        // Titelzeile für diese Spalte befüllen
                        add_filter(
                            $filter_for_title,
                            function ( $columns ) {
                                $current_screen    = get_current_screen();
                                $current_post_type = $current_screen->post_type;

                                foreach ( BetterBackend::$add_post_list_columns[ $current_post_type ] as $slug => $column ) {
                                    $columns[ $slug ] = $column['title'];
                                }

                                return $columns;
                            }
                        );

                        // Content für diese Spalte ausgeben
                        add_filter(
                            $filter_for_content,
                            function ( $slug, $post_id ) {
                                $current_screen    = get_current_screen();
                                $current_post_type = $current_screen->post_type;

                                if ( isset( BetterBackend::$add_post_list_columns[ $current_post_type ][ $slug ] ) && is_callable(
                                        BetterBackend::$add_post_list_columns[ $current_post_type ][ $slug ]['callback']
                                    )
                                ) {
                                    call_user_func(
                                        BetterBackend::$add_post_list_columns[ $current_post_type ][ $slug ]['callback'],
                                        $post_id
                                    );
                                }
                            },
                            10,
                            2
                        );
                    }
                }
            );
        }
    }


    /**
     * Aktiviert Suchmaschinenoptimierung für einen bestimmten Post-Typ
     *
     * Beinhaltet Felder für die Meta Description und ein Vorschaubild für
     * Facebook und Twitter.
     *
     * @param mixed $post_type Post-Typ (oder Array aus Post-Typen), für den SEO aktiviert wird
     */
    public static function use_seo_features( $post_type ) {
        // abbrechen, wenn ACF nicht installiert ist
        if ( ! class_exists( 'acf' ) ) {
            return;
        }

        self::$use_seo_features = self::join_array( self::$use_seo_features, $post_type );

        if ( ! self::done( 'use_seo_features' ) ) {
            add_action(
                'registered_post_type',
                function ( $post_type ) {
                    $acf_location = array();

                    foreach ( BetterBackend::$use_seo_features as $post_type ) {
                        $acf_location[] = array(
                            array( // ACF erwartet doppelte Verschachtelung
                                'param'    => 'post_type',
                                'operator' => '==',
                                'value'    => $post_type,
                            ),
                        );
                    }

                    BetterBackend::register_acf_seo_fields( $acf_location );
                    add_action( 'wp_head', array( __CLASS__, 'wp_head_seo_tags' ) );
                }
            );
        }
    }

    private static function register_acf_seo_fields( $location ) {
        if ( ! function_exists( 'include_field_types_limiter' ) ) {
            require_once dirname( __FILE__ ) . '/acf-limiter-field/acf-limiter.php';
        }

        register_field_group(
            array(
                'key'                   => 'group_54116419e11ae',
                'title'                 => 'Teaser, Suchmaschinen- und Social Media-Optimierung',
                'fields'                => array(
                    array(
                        'key'                => 'field_5418376218afcc',
                        'label'              => 'Title',
                        'name'               => '_qundg_meta_title',
                        'prefix'             => '',
                        'type'               => 'limiter',
                        'instructions'       => 'Zwischen 50 & 60 Zeichen. Erscheint oben im Tab, bei Google in den Suchergebnissen und beim Teilen auf Facebook und Twitter.',
                        'required'           => 0,
                        'conditional_logic'  => 0,
                        'rows'               => 1,
                        'character_number'   => 60,
                        'display_characters' => 0,
                    ),
                    array(
                        'key'                => 'field_541164218afcc',
                        'label'              => 'Teaser',
                        'name'               => '_qundg_meta_description',
                        'prefix'             => '',
                        'type'               => 'limiter',
                        'instructions'       => 'Bis zu 156 Zeichen. Erscheint auf Übersichtsseiten, bei Google in den Suchergebnissen und beim Teilen auf Facebook und Twitter.',
                        'required'           => 1,
                        'conditional_logic'  => 0,
                        'character_number'   => 156,
                        'display_characters' => 0,
                    ),
                    array(
                        'key'               => 'field_54181201da486',
                        'label'             => 'Vorschaubild für Facebook & Twitter',
                        'name'              => '_qundg_social_media_vorschaubild',
                        'prefix'            => '',
                        'type'              => 'image',
                        'instructions'      => 'Wird bei Facebook bzw. Twitter angezeigt, wenn jemand diese Seite teilt, liket oder tweetet.<br><br>
Mindestens 200x200 Pixel<br>
Höchstens 1 MB Dateigröße<br>
Optimales Seitenverhältnis: 1.91:1 für Facebook, 4:3 für Twitter &ndash; je nachdem, wem man es recht machen will.',
                        'required'          => 0,
                        'conditional_logic' => 0,
                        'return_format'     => 'url',
                        'preview_size'      => 'medium',
                        'library'           => 'all',
                    ),
                ),
                'location'              => $location,
                'menu_order'            => 9999,
                'position'              => 'normal',
                'style'                 => 'default',
                'layout'                => 'default',
                'label_placement'       => 'top',
                'instruction_placement' => 'label',
                'hide_on_screen'        => '',
            )
        );
    }

    public static function wp_head_seo_tags() {
        if ( is_singular() ) {
            global $post;

            // meta description ausgeben
            $meta_description_field = get_field( '_qundg_meta_description', $post->ID );
            if ( $meta_description_field ) {
                // custom field "meta description" wurde ausgefüllt
                $meta_description = strip_tags( trim( htmlspecialchars( $meta_description_field ) ) );
            } else {
                if ( strlen( $post->post_content ) > 156 ) {
                    // content ist länger als 156 Zeichen und wird gekappt
                    $meta_description = substr( strip_tags( trim( htmlspecialchars( $post->post_content ) ) ),
                            0,
                            152 ) . ' ...';
                } else {
                    // content ist kürzer als 156 Zeichen und wird komplett ausgegeben
                    $meta_description = strip_tags( trim( htmlspecialchars( $post->post_content ) ) );
                }
            }

            if ( $meta_description ) {
                ?>
                <meta name="description" content="<?php echo $meta_description; ?>">
                <meta property="og:description" content="<?php echo $meta_description; ?>">
                <meta property="twitter:description" content="<?php echo $meta_description; ?>">
                <?php
            }


            // vorschaubild fuer facebook und twitter einbinden
            $social_media_image = get_field( '_qundg_social_media_vorschaubild' );
            if ( $social_media_image ) {
                ?>
                <meta property="og:image" content="<?php echo $social_media_image; ?>">
                <meta name="twitter:image" content="<?php echo $social_media_image; ?>">
                <?php
            }

            // Meta Title
            $meta_title_field = get_field( '_qundg_meta_title', $post->ID );

            if ( $meta_title_field ) {
                ?>
                <meta property="og:title" content="<?php echo esc_attr( $meta_title_field ); ?>">

                <?php
            } else {

                // spezielle meta-infos fuer facebook und twitter
                ?>
                <meta property="og:title" content="<?php echo esc_attr( $post->post_title ); ?>">
                <?php
            }
            ?>
            <meta property="og:title" content="<?php echo esc_attr( $post->post_title ); ?>">
            <meta property="og:site_name" content="<?php bloginfo( 'name' ); ?>">
            <meta name="twitter:card" content="summary">
            <meta name="twitter:title" content="<?php echo esc_attr( $post->post_title ); ?>">
            <?php
        }
    }


    /**
     * Gibt für einen Post-Typ bei the_excerpt() die Meta Description aus
     *
     * Funktioniert nur, wenn vorher self::use_seo_features() für denselben
     * Post-Typ aktiviert wurde. Falls die Meta Description leer ist, wird der
     * normale Excerpt ausgegeben.
     *
     * @param mixed $post_type Post-Typ (oder Array aus Post-Typen), für den die Meta Description als Excerpt
     *     verwendet wird
     *
     * @throws Exception Wenn für einen Post-Typen nicht vorher self::use_seo_features() aufgerufen wurde
     */
    public static function use_meta_description_as_excerpt( $post_type ) {
        // abbrechen, wenn ACF nicht installiert ist
        if ( ! class_exists( 'acf' ) ) {
            return;
        }

        if ( is_array( $post_type ) ) {
            foreach ( $post_type as $type ) {
                if ( ! in_array( $type, self::$use_seo_features ) ) {
                    throw new Exception(
                        'BetterBackend::use_meta_description_as_excerpt() funktioniert nur nach Aufruf von BetterBackend::use_seo_features() für denselben Post-Typ.'
                    );
                }
            }
        }

        self::$use_meta_description_as_excerpt = self::join_array(
            self::$use_meta_description_as_excerpt,
            $post_type
        );

        if ( ! self::done( 'use_meta_description_as_excerpt' ) ) {
            add_filter(
                'get_the_excerpt',
                function ( $excerpt ) {
                    global $post;
                    if ( in_array( $post->post_type, BetterBackend::$use_meta_description_as_excerpt ) ) {
                        $meta_description = get_field( '_qundg_meta_description', $post->ID );
                        if ( $meta_description ) {
                            return $meta_description;
                        }
                    }

                    return $excerpt;
                }
            );
        }
    }


    /**
     * Deaktiviert eine eingebaute Taxonomie vollständig
     *
     * @param mixed $taxonomy Slug der zu deaktivierenden Taxonomie (oder ein Array aus Slugs)
     */
    public static function disable_taxonomy( $taxonomy ) {
        self::$disable_taxonomies = self::join_array( self::$disable_taxonomies, $taxonomy );

        if ( ! self::done( 'disable_taxonomies' ) ) {
            add_action(
                'init',
                function () {
                    foreach ( BetterBackend::$disable_taxonomies as $taxonomy ) {
                        register_taxonomy( $taxonomy, array() );
                    }
                }
            );
        }
    }


    public static function customize_login_form() {
        if ( ! self::done( 'customize_login_form' ) ) {
            add_action(
                'login_enqueue_scripts',
                function () {
                    echo '
                        <style type="text/css">
                            .login h1 a {
                                background-image: url(' . plugins_url( 'qundg-logo-large.png', __FILE__ ) . ') !important;
                                background-size: 300px 38px !important;
                                width: auto !important;
                                height: 38px !important;
                            }
                        </style>';
                }
            );
        }
    }


    /**
     * Ersetzt vordefinierte durch eigene Trenner/Separatoren im Admin-Menü
     *
     * Bei Nutzung dieser Funktion werden erst alle vordefinierten Trenner
     * entfernt und dann die eigenen eingefügt.
     *
     * @param mixed $after Menüpunkt (oder Array aus Menüpunkten), nach dem ein Trenner eingesetzt wird, z.B.
     *     'edit.php?post_type=page'
     */
    public static function add_menu_separator( $after ) {
        self::$add_menu_separators = self::join_array( self::$add_menu_separators, $after );

        // jetzt eigene Trenner einsetzen
        if ( ! self::done( 'add_menu_separators' ) ) {
            add_action(
                'admin_menu',
                function () {
                    global $menu;
                    $menu = array_values( $menu ); // sorgt dafür, dass Keys lückenlos nummeriert sind

                    // erst alle Trenner entfernen
                    foreach ( $menu as $offset => $section ) {
                        if ( substr( $section[2], 0, 9 ) == 'separator' ) {
                            array_splice( $menu, $offset, 1 );
                        }
                    }

                    // jetzt eigene Trenner einsetzen
                    $index = 1; // Trenner müssen in Wordpress durchnummeriert sein
                    foreach ( BetterBackend::$add_menu_separators as $after ) {
                        foreach ( $menu as $offset => $section ) {
                            if ( $section[2] == $after ) {
                                array_splice(
                                    $menu,
                                    $offset + 1,
                                    0,
                                    array( array( '', 'read', 'separator' . $index ++, '', 'wp-menu-separator' ) )
                                );
                                break;
                            }
                        }
                    }
                },
                9999999
            );
        }
    }


    /**
     * Verschiebt ein Element im Admin-Menü
     *
     * @param string $move Zu verschiebendes Element, z.B. 'edit.php'
     * @param string $after Menüpunkt, hinter den das Element verschoben wird, z.B. 'edit.php'
     */
    public static function move_menu_item( $move, $after ) {
        self::$move_menu_items[ $move ] = $after;

        if ( ! self::done( 'move_menu_items' ) ) {
            add_action(
                'admin_menu',
                function () {
                    global $menu;
                    $menu = array_values( $menu ); // sorgt dafür, dass Keys lückenlos nummeriert sind

                    foreach ( BetterBackend::$move_menu_items as $move => $after ) {
                        $to_be_moved = false;

                        // Menüpunkt zwischenspeichern und an alter Stelle entfernen
                        foreach ( $menu as $offset => $section ) {
                            if ( $section[2] == $move ) {
                                $to_be_moved = $section;
                                array_splice( $menu, $offset, 1 );
                                break;
                            }
                        }

                        // Menüpunkt an neuer Stelle einfügen
                        if ( $to_be_moved ) {
                            foreach ( $menu as $offset => $section ) {
                                if ( $section[2] == $after ) {
                                    array_splice( $menu, $offset + 1, 0, array( $to_be_moved ) );
                                    break;
                                }
                            }
                        }
                    }
                },
                9999998
            );
        }
    }


    /**
     * Entfernt ein Element aus dem Admin-Menü
     *
     * @param mixed $url Zu entfernender Menüpunkt (oder Array aus Menüpunkten), z.B. 'edit.php'
     */
    public static function remove_menu_item( $url ) {
        self::$remove_menu_items = self::join_array( self::$remove_menu_items, $url );

        if ( ! self::done( 'remove_menu_items' ) ) {
            add_action(
                'admin_menu',
                function () {
                    global $menu;
                    $menu = array_values( $menu ); // sorgt dafür, dass Keys lückenlos nummeriert sind

                    foreach ( $menu as $offset => $section ) {
                        if ( in_array( $section[2], BetterBackend::$remove_menu_items ) ) {
                            unset( $menu[ $offset ] );
                        }
                    }
                },
                9999
            );
        }
    }


    /**
     * Benennt einen Eintrag im Adminmenü um
     *
     * @param string $url Datei des Menüpunktes, z.B. 'nav-menus.php'
     * @param string $new_name Neuer Name für den Menüpunkt
     */
    public static function rename_menu_item( $url, $new_name ) {
        self::$rename_menu_items[ $url ] = $new_name;

        if ( ! self::done( 'rename_menu_items' ) ) {
            add_action(
                'admin_menu',
                function () {
                    global $menu;

                    foreach ( $menu as &$section ) {
                        if ( array_key_exists( $section[2], BetterBackend::$rename_menu_items ) ) {
                            $section[0] = BetterBackend::$rename_menu_items[ $section[2] ];
                        }
                    }
                },
                9999996
            );
        }
    }


    /**
     * Fügt dem Admin-Menü ein Element hinzu
     *
     * @param string $url Datei des Menüpunktes, z.B. 'nav-menus.php'
     * @param string $after Datei des Menüpunktes, nach dem der neue Punkt eingefügt wird, z.B. 'edit.php'
     * @param string $title Bezeichnung des neuen Menüpunktes
     * @param string $capability Erforderliche Capability zum Zugriff auf diesen Menüpunkt
     * @param string $icon Icon für den Menüpunkt, z.B. 'dashicons-menu'
     */
    public static function add_menu_item( $url, $after, $title, $capability, $icon = 'dashicons-admin-post' ) {
        self::$add_menu_items[] = array(
            'url'        => $url,
            'after'      => $after,
            'title'      => $title,
            'capability' => $capability,
            'icon'       => $icon,
        );
        self::move_menu_item( $url, $after );

        if ( ! self::done( 'add_menu_items' ) ) {
            add_action(
                'admin_menu',
                function () {
                    foreach ( BetterBackend::$add_menu_items as $item ) {
                        add_menu_page(
                            $item['url'],
                            $item['title'],
                            $item['capability'],
                            $item['url'],
                            null,
                            $item['icon']
                        );
                    }
                },
                9999996
            );
        }
    }


    /**
     * Fügt dem Admin-Menü ein Submenü-Element hinzu
     *
     * @param string $url Datei des Menüpunktes, z.B. 'edit-comments.php'
     * @param string $parent Datei des Hauptmenüpunktes, unter dem der neue Punkt eingefügt wird, z.B. 'edit.php'
     * @param string $title Bezeichnung des neuen Menüpunktes
     * @param string $capability Erforderliche Capability zum Zugriff auf diesen Menüpunkt
     */
    public static function add_submenu_item( $url, $parent, $title, $capability ) {
        self::$add_submenu_items[] = array(
            'url'        => $url,
            'parent'     => $parent,
            'title'      => $title,
            'capability' => $capability,
        );

        if ( ! self::done( 'add_submenu_items' ) ) {
            add_action(
                'admin_menu',
                function () {
                    foreach ( BetterBackend::$add_submenu_items as $item ) {
                        add_submenu_page(
                            $item['parent'],
                            $item['title'],
                            $item['title'],
                            $item['capability'],
                            $item['url']
                        );
                    }
                },
                9999997
            );
        }
    }

    /**
     * Loads scripts with ansynchrounous tag
     */
    public static function load_scripts_asnychronous() {

        add_filter( 'clean_url',
            function ( $url ) {

                $extension = pathinfo( parse_url( $url, PHP_URL_PATH ), PATHINFO_EXTENSION );
                if ( $extension == 'js' ) {

                    $url .= "' async='async";
                }

                return $url;
            },
            11,
            1 );
    }


}
