### Better Backend Dokumentation

*Neu: Version 1.2.3 - Jetzt kompatibel für PHP 7!*

#### Was macht Better Backend

Dieses Plugin räumt das WordPress Backend auf. Du kannst das Backend so verändern, um nur noch gewünschte Punkte darzustellen. Des Weiteren kann die Reihenfolge des Menüs links verändert werden, einzelne Punkte können in andere Punkte verschoben werden. Im Dashboard können Shortlinks wie z.B. „Zu den Projekten“ angelegt werden. 

Für jedes Post wird ein Custom Fields für Meta-Description und Meta-Title angelegt. Zusätzlich können Meta-Infos für Facebook und Twitter hinterlegt werden, die im Head automatisch ausgespielt werden.

In unserem Thema-Ordner im includes-Order gibt es die betterbackend.php - die Konfiguration für Better Backend. Jede Funktion die gewünscht wird, muss hier aufgerufen werden.

#### Methoden

`clean_wp_head`
Diese Funktion entfernt verschiedene Funktionen aus der `wp_head`-Funktion von WordPress. Damit werden verschiedene Standard-Meta-Tags aus dem Quellcode entfernt.
Auch die Rest-API wird sicherheitshalber gekillt, wenn du sie verwenden möchtest gheht das so: `BetterBackend::disable_rest_api = false`, natürlich vor `etterBackend::clean_wp_head()

`add_feed_meta`
Mit dieser Funktion können benutzerdefinierte RSS und Atom Feedlinks hinzugefügt werden. Diese wurden vorher per `clean_wp_head` ggf. entfernt.

`remove_post_meta_box`
Entfernt WordPress-Meta-Boxen im Post wie z.B. den Post-Type.

`tinymce_remove_button`
Entfernt einzelne Buttons aus dem WordPress Tiny-MCE-Editor.

`tinymce_add_button`
Fügt an einer gewünschten Stelle Buttons hinzu.

`clean_dashboard`
Diese Funktion räumt das Dashboard auf.

`add_custom_dashboard_item`
Mit dieser Funktion kann man dem Dashboard eigene Boxen hinzufügen.

`add_custom_dashboard_video`
Mit dieser Funktion kann man Videos im Dashboard einfügen, wenn man z.B. Erklär-Videos einbinden möchte.

`track_password_changes`
Die Funktion zählt wie oft der Benutzer sein Passwort geändert hat und gibt ggf. einen Hinweis, wenn man sein Passwort mal ändern sollte.

`remove_admin_bar_item`
Entfernt Menüpunkte aus der Admin-Bar. Diese kann man anhand ihres Slugs entfernen.

`custom_admin_footer`
Passt den Footer im Dashboard an um z.B. die Version oder den Copy-Text zu entfernen und einen qundg-Hinweis hinzufügen.

`clean_profile_edit_form`
Passt im Benutzerprofil die möglichen Einstellungen an.

`remove_post_order_form`
Entfernt bei Seiten die Einstellung zur Seiten-Reihenfolge (Page Order).

`remove_post_publish_form`
Damit kann man die Publish-Box in Beiträgen/Seiten anpassen.

`customize_user_roles`
Generiert den Super-Redakteur und entfernt Contributor und Subscriber.

`remove_post_list_feature` und `remove_post_list_column`
In der Postübersicht können damit Spalten und Hover-Punkte entfernt werden.

`add_post_list_column`
Fügt einzelne Spalten in einer Postübersicht hinzu.

`use_seo_features`
Fügt die Meta-Boxen für Meta-Description und Meta-Infos für Facebook/Twitter hinzu.

`wp_head_seo_tag`
Diese Funktion schreibt die Tags von `use_seo_features` in den Quellcode

`use_meta_description_as_excerpt`
Überschreibt den WordPress-Excerpt mit dem Inhalt der Meta-Description-Box.

`disable_taxonomy`
Ermöglicht eingebaute Taxonomien vollständig zu entfernen.

`customize_login_form`
Ermöglicht es, dass Login-Form von WordPress anzupassen.

`add_menu_seperator`
Fügt im Menü einen kleinen Abstand zwischen den einzelnen Menüpunkten hinzu.

`move_menu_item`
Verschiebt gewünschte Menüpunkt an eine andere Stelle.

`remove_menu_item`
Entfernt gewünschte Menüpunkte.

`rename_menu_item`
Benennt gewünschte Menüpunkte um.

`add_menu_item`
Fügt gewünschte Menüpunkte hinzu.

`add_submenu_item`
Fügt dem Admin-Menü ein Submenü-Element hinzu.